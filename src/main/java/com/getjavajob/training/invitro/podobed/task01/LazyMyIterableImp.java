package com.getjavajob.training.invitro.podobed.task01;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

/**
 * Created by podobeda on 31.08.2016 22:57
 */
public class LazyMyIterableImp<E> extends MyIterableImpl<E> {
    private MyIterableImpl<E> myIterable;
    private Status status;

    public LazyMyIterableImp(MyIterableImpl<E> myIterable, Status status) {
        this.status = status;
        this.myIterable = myIterable;
    }

    @SafeVarargs
    public static <T> MyIterable<T> of(T... args) {
        return MyIterableImpl.of(args);
    }

    protected MyIterable<E> eagerFilter(@Nonnull Predicate<E> predicate) {
        MyIterableImpl<E> deep = (MyIterableImpl<E>) calling(status.getMeth());
        return deep != null ? deep.eagerFilter(predicate) : null;
    }

    private MyIterable calling(LazyMeth meth) {
        switch (meth) {
            case FILTER:
                return myIterable.eagerFilter(status.getPredicate());
            case TRANSFORM:
                return myIterable.eagerTransform(status.getFunction());
            default:
                break;
        }
        return null;
    }

    protected <T> MyIterable<T> eagerTransform(@Nonnull Function<E, T> transformer) {
        MyIterableImpl<E> deep = (MyIterableImpl<E>) calling(status.getMeth());
        return deep != null ? deep.eagerTransform(transformer) : null;
    }

    @Override
    public <T> T aggregate(@Nullable T initValue, @Nonnull Aggregator<E, T> aggregator) {
        MyIterableImpl<E> deep = (MyIterableImpl<E>) calling(status.getMeth());
        return (T) (deep != null ? deep.aggregate(initValue, aggregator) : null);
    }

    @Override
    public SortedSet<E> toSet(@Nonnull Comparator<E> comarator) {
        return myIterable.toSet(comarator);
    }

    @Override
    public List<E> toList() {
        return myIterable.toList();
    }

    @Nullable
    @Override
    public E findFirst(@Nonnull Predicate<E> predicate) {
        MyIterableImpl<E> deep = (MyIterableImpl<E>) calling(status.getMeth());
        return deep != null ? deep.findFirst(predicate) : null;
    }

    @Override
    public Iterator<E> iterator() {
        return myIterable.iterator();
    }
}
