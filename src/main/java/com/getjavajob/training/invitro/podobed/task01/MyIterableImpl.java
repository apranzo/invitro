package com.getjavajob.training.invitro.podobed.task01;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

/**
 * Created by podobeda on 31.08.2016 20:37
 */
public class MyIterableImpl<E> implements MyIterable<E> {

    private List<E> array;

    public MyIterableImpl() {
    }

    public MyIterableImpl(List<E> array) {
        this.array = array;
    }

    @SafeVarargs
    public static <E> MyIterable<E> of(E... args) {
        ArrayList<E> array = new ArrayList<>();
        Collections.addAll(array, args);
        return new MyIterableImpl<>(array);
    }

    @Override
    public MyIterable<E> filter(@Nonnull Predicate<E> predicate) {
        return new LazyMyIterableImp(this, new Status(LazyMeth.FILTER, predicate));
    }

    protected MyIterable<E> eagerFilter(@Nonnull Predicate<E> predicate) {
        ArrayList<E> filtred = new ArrayList<>();
        for (E e : this.array) {
            if (predicate.apply(e)) {
                filtred.add(e);
            }
        }
        return new MyIterableImpl<>(filtred);
    }

    protected <T> MyIterable<T> eagerTransform(@Nonnull Function<E, T> transformer) {
        ArrayList<T> transformed = new ArrayList<>();
        for (E e : this.array) {
            transformed.add(transformer.apply(e));
        }
        return new MyIterableImpl<>(transformed);
    }

    @Override
    public <T> MyIterable<T> transform(@Nonnull Function<E, T> transformer) {
        return new LazyMyIterableImp(this, new Status(LazyMeth.TRANSFORM, transformer));
    }

    @Override
    public <T> T aggregate(@Nullable T initValue, @Nonnull Aggregator<E, T> aggregator) {
        for (E e : this.array) {
            initValue = aggregator.apply(initValue, e);
        }
        ;
        return initValue;
    }

    @Override
    public SortedSet<E> toSet(@Nonnull Comparator<E> comarator) {
        return null;
    }

    @Override
    public List<E> toList() {
        return null;
    }

    @Nullable
    @Override
    public E findFirst(@Nonnull Predicate<E> predicate) {
        E result = null;
        for (E e : this.array) {
            if (predicate.apply(e)) {
                result = e;
                break;
            }
        }
        return result;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    protected enum LazyMeth {
        FILTER,
        TRANSFORM,
    }

    protected class Status<T> {
        private LazyMeth meth;
        private Predicate<? extends E> predicate;
        private Function function;

        public Status(LazyMeth meth, Predicate<E> predicate) {
            this.setMeth(meth);
            this.predicate = predicate;
        }

        public Status(LazyMeth meth, Function<? extends E, ? extends T> function) {
            this.setMeth(meth);
            this.function = function;
        }

        public LazyMeth getMeth() {
            return meth;
        }

        public void setMeth(LazyMeth meth) {
            this.meth = meth;
        }

        public Predicate getPredicate() {
            return predicate;
        }

        public Function getFunction() {
            return function;
        }
    }
}
